const mongoose = require('mongoose');

const ServiceSchema = mongoose.Schema({
    nameService: String
});

module.exports = mongoose.model('Service', ServiceSchema);
