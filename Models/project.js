const mongoose = require('mongoose');
const TaskSchema = require('./task');
const MilestoneSchema = require('./milestone');

const ProjectSchema = mongoose.Schema({
    name: String,
    desc: String,
    task: [TaskSchema],
    milestones: [MilestoneSchema]
});

module.exports = mongoose.model('Project', ProjectSchema);
