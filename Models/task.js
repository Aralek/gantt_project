const mongoose = require('mongoose');
const ProjectSchema = require("./project");
const RessourceSchema = require('./ressource');

const TaskSchema = mongoose.Schema({
    _id: Number,
    project: ProjectSchema,
    name: String,
    desc: String,
    start: Date,
    end: Date,
    percentageProgress: Number,
    color: String,
    linkedTask: [this],
    ressources: RessourceSchema
});

module.exports = mongoose.model('Task', TaskSchema);

