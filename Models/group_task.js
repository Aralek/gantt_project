const mongoose = require('mongoose');
const ProjectSchema = require("./project");

const GroupTaskSchema = mongoose.Schema({
    project: ProjectSchema,
    start: Date,
    end: Date
});

module.exports = mongoose.model('GroupTask', GroupTaskSchema);
