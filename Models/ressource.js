const mongoose = require('mongoose');
const ProjectSchema = require("./project");

const RessourceSchema = mongoose.Schema({
    project: ProjectSchema,
    name: String,
    cost: Number,
    type: String
});

module.exports = mongoose.model('Ressource', RessourceSchema);
