const mongoose = require('mongoose');
const ProjectSchema = require("./project");

const DaysOffSchema = mongoose.Schema({
    project: ProjectSchema,
    Mo: Boolean,
    Tu: Boolean,
    We: Boolean,
    Th: Boolean,
    Fr: Boolean,
    Sa: Boolean,
    Su: Boolean
});

module.exports = mongoose.model('DaysOff', DaysOffSchema);
