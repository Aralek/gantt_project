const mongoose = require('mongoose');
const ProjectSchema = require("./project");

const MilestoneSchema = mongoose.Schema({
    project: ProjectSchema,
    name: String,
    date: Date
});

module.exports = mongoose.model('Milestone', MilestoneSchema);
