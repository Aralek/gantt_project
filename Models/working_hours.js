const mongoose = require('mongoose');
const ProjectSchema = require("./project");

const WorkingHoursSchema = mongoose.Schema({
    project: ProjectSchema,
    start: Date,
    end: Date
});

module.exports = mongoose.model('WorkingHours', WorkingHoursSchema);
