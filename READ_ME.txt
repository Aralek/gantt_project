Before launching the project, you should copy and past the environment file (check your mail) in the root folder :

Then install the following librairies : 
- express
- dotenv
- socket.io

You should have this : 

gantt_project/
    .env
    .gitignore
    index.js
    package.json
    READ_ME.txt
    Client/
        ...
    Models/
        ...
    node_modules/
        ...
