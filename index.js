'use strict';

const dotenv = require('dotenv');
dotenv.config();

const db_url = process.env.DB_URL;

const express = require('express');
const app = express();
const http = require('http').Server(app);
const bodyParser = require("body-parser");
const path = require('path');
app.use(bodyParser.json());

const socket = require('socket.io-client');
let client = socket.connect('http://51.15.137.122:18000/', {reconnect: true});

console.log("Start program");

app.use(express.static(path.join(__dirname, 'client')));

http.listen(3000, () => {

    console.log("Listening on port 3000");

    client.on('connect', () => {
        console.log('connected');
    
        let help = client.emit('needHelp');
        //console.log(help);
    });

});
